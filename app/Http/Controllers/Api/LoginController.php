<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Google\Client;
use Google_Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authPassword(Request $request): JsonResponse
    {
        $validator = Validator::make($request->json()->all(), [
            'email' => 'required|email',
            'password' => 'required',
            'device_name' => 'required', // TODO: should be get from header data
        ]);

        if ($validator->fails()) {
            return response()->json($validator->getMessageBag(), 400);
        }

        $validated = $validator->validated();
        $user = User::where('email', $validated['email'])->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json(["error" => 'Email atau Password anda tidak cocok!'], 503);
        }

        return response()->json([
            'token' => $user->createToken($request->device_name)->plainTextToken
        ]);
    }

    /**
     * Authenticate using Google Email and UID
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function authGoogle(Request $request) : JsonResponse
    {
        $validator = Validator::make($request->json()->all(), [
            'email' => 'required|email',
            'uid' => 'required',
            'device_name' => 'required', // TODO: should be get from header data
        ]);

        if ($validator->fails()) {
            return response()->json($validator->getMessageBag(), 400);
        }

        $validated = $validator->validated();

        $user = User::where('email', $validated['email'])->first();

        if (!$user) {
            return response()->json(["error" => 'Anda belum terdaftar!'], 503);
        } elseif ($validated['uid'] != $user->google_uid) {
            return response()->json(["error" => 'Invalid UID'], 503);
        }

        return response()->json([
            'token' => $user->createToken($request->device_name)->plainTextToken
        ]);
    }

    // public function loginGoogle() {
    //     // TODO: Put to .env
    //     $google = [
    //         'client_id' => "580450841598-92s286j7rc7suo86m40vmgo6ohgcm6dm.apps.googleusercontent.com",
    //         'client_secret' => "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",
    //         'redirect_url' => "http://javacode.io"
    //     ];

    //     $client = new Client();
    //     $client->setApplicationName("Login to Java Code");
    //     $client->addScope(\Google\Service\Oauth2::USERINFO_EMAIL);
    //     $client->setClientId($google['client_id']);
    //     $client->setClientSecret($google['client_secret']);
    //     $client->setRedirectUri($google['redirect_url']);

    //     $auth_url = $client->createAuthUrl();

    //     header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
    //     $client->setPrompt('consent');
    // }
}
