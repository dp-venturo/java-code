<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function getProfile(Request $request) : JsonResponse {
        if(!$request->user()) {
            return response()->json(['error' => 'Invalid authorization token!'],503);
        }

        // Eager loading
        $request->user()->profile;

        return response()->json($request->user());
    }
}
