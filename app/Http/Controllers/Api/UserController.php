<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    /**
     * Retrieve all user data
     *
     * @return JsonResource
     */
    public function all() : JsonResource {
        return new UserCollection(User::all());
    }

    /**
     * Show single user information
     *
     * @param integer $user_id
     * @return JsonResource
     */
    public function show(int $user_id): JsonResource {
        return new UserResource(User::findOrFail($user_id));
    }

    /**
     * Create new user
     *
     * @param Request $request
     * @return UserResource|JsonResponse
     */
    public function create(Request $request): UserResource|JsonResponse
    {
        $validator = Validator::make($request->json()->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email:rfc,dns|unique:users',
            'password' => ['required', Password::min(8)],
            'address' => 'max:255',
            'phone' => 'max:20',
        ]);

        if ($validator->fails())
            return response()->json($validator->getMessageBag(), 400);

        // Insert to User
        $insert = $validator->validated();
        $insert['password'] = Hash::make($insert['password']);
        $user = User::create($insert);

        // Create to Profile
        $insert['user_id'] = $user->id;
        Profile::create($insert);

        return new UserResource($user);
    }

    /**
     * Update user information
     *
     * @param Request $request
     * @param integer $user_id
     * @return JsonResponse
     */
    public function update(Request $request, int $user_id = null): JsonResponse
    {
        $user = User::findOrFail($user_id);

        // TODO: Sanitize sensitive data (password, etc)
        $user->update($request->json()->all());

        return response()->json(['message' => 'Tersimpan']);
    }

    /**
     * Delete user
     *
     * @param integer $user_id
     * @return JsonResponse
     */
    public function delete(int $user_id): JsonResponse {
        $user = User::findOrFail($user_id);
        $user->delete();

        return response()->json(['message' => 'Dihapus']);
    }
}
