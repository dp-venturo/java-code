import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiscountPageRoutingModule } from './discount-page-routing.module';
import { DiscountPageComponent } from './discount-page.component';


@NgModule({
  declarations: [ DiscountPageComponent ],
  imports: [
    CommonModule,
    DiscountPageRoutingModule
  ]
})
export class DiscountPageModule { }
