import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiscountPageComponent } from './discount-page.component';


const routes: Routes = [{
  path: '',
  component: DiscountPageComponent,
  data: {
    title: `Discount Management`
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiscountPageRoutingModule { }
