import { Component, OnInit, ViewChild } from '@angular/core';
import { JavacodeService } from '../services/javacode.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  constructor(private javacodeApi: JavacodeService) { }

  users: any[];
  registration = {
    name: "",
    email: "",
    password: "",
    address: "",
    phone: ""
  };

  editCache = {
    id: 0,
    name: "",
    email: "",
    password: "",
    address: "",
    phone: "",
    profile: {
      address: "",
      phone: ""
    }
  };

  ngOnInit(): void {
    // Get database
    this.javacodeApi.getUsers().subscribe((response: any) => {
      this.users = response.data;
    });
  }

  register() {
    this.javacodeApi.postUser(this.registration).subscribe((response: any) => {
      this.users.push(response.data);

      // Reset
      this.registration = {
        name: "",
        email: "",
        password: "",
        address: "",
        phone: ""
      };
    });
  }

  edit(user: any) {
    this.editCache = user;

    // TODO: Profile field
  }

  save() {
    this.javacodeApi.patchUser(this.editCache.id, this.editCache).subscribe((response: any) => {
      // TODO: Show
    });
  }

  delete(user_id: number) {
    this.javacodeApi.deleteUser(user_id).subscribe((response: any) => {
      let index = this.users.findIndex(e => e.id === user_id);
      this.users.splice(index, 1);
    });
  }
}
