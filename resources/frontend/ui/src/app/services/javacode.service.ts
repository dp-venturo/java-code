import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class JavacodeService {
  constructor(private http: HttpClient, private router: Router) {
    const apiKey = localStorage.getItem('apiKey');

    if (apiKey == undefined || apiKey == "") { // User never login before
      this.router.navigate(['/login']);
      return;
    }

    this.setApiKey(apiKey);
  }

  url = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Error processing
  processError(err: any) {
    let message = '';
    if (err.error instanceof ErrorEvent) {
      message = err.error.message;
    } else {
      message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }

    // TODO: Remove debug and Generate Notification
    console.log(message);
    console.log("Data:", err.error);

    return throwError(() => {
      message;
    });
  }

  // Authentication using password
  authPassword(email: string, password: string) {
    const data = {
      email: email,
      password: password,
      device_name: "Angular"
    };

    return this.http.post(this.url + "/auth/password", data, this.httpOptions)
      .pipe(retry(1), catchError(this.processError));
  }

  // Authentication using google api
  authGoogle(email: string, uid: string) {
    const data = {
      email: email,
      uid: uid,
      device_name: "Angular"
    };

    return this.http
      .post(this.url + "/auth/google", data, this.httpOptions)
      .pipe(retry(1), catchError(this.processError));
  }

  // Setup apiKey to header
  setApiKey(apiKey: string) : void {
    if (!this.httpOptions.headers.has("Authorization"))  {
      this.httpOptions.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': "Bearer " + apiKey
      });
    }
  }

  // Get user profile info
  getProfile() {
    return this.http.get(this.url + "/profile", this.httpOptions)
      .pipe(retry(1), catchError(this.processError));
  }

  /**
   * User Management
   */
  // Register new user
  postUser(body: any) {
    return this.http.post(this.url + '/user', body, this.httpOptions)
      .pipe(retry(1), catchError(this.processError));
  }

  // Get all user list
  getUsers() {
    return this.http.get(this.url + "/user", this.httpOptions)
      .pipe(retry(1), catchError(this.processError));
  }

  // Get single user data
  getUser(user_id: number) {
    return this.http.get(this.url + '/user/' + user_id, this.httpOptions)
      .pipe(retry(1), catchError(this.processError));
  }

  // Update data
  patchUser(user_id: number, body: any) {
    return this.http.patch(this.url + '/user/' + user_id, body, this.httpOptions)
      .pipe(retry(1), catchError(this.processError));
  }

  deleteUser(user_id: number) {
    return this.http.delete(this.url + '/user/' + user_id, this.httpOptions)
      .pipe(retry(1), catchError(this.processError));
  }
}
