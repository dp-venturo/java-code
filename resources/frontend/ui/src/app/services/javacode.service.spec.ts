import { TestBed } from '@angular/core/testing';

import { JavacodeService } from './javacode.service';

describe('JavacodeService', () => {
  let service: JavacodeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JavacodeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
