import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromotionPageComponent } from './promotion-page.component';


const routes: Routes = [{
  path: '',
  component: PromotionPageComponent,
  data: {
    title: `Users`
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionPageRoutingModule { }
