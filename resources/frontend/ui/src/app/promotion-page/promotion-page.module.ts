import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionPageRoutingModule } from './promotion-page-routing.module';
import { PromotionPageComponent } from './promotion-page.component';


@NgModule({
  declarations: [ PromotionPageComponent ],
  imports: [
    CommonModule,
    PromotionPageRoutingModule
  ]
})
export class PromotionPageModule { }
