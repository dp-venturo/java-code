import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { JavacodeService } from './services/javacode.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private javacodeApi: JavacodeService) { }

  login(email: string, password: string) {
    this.afAuth.signInWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Nice, it worked!');
        this.router.navigateByUrl('/profile');
      })
      .catch(err => {
        console.log('Something went wrong: ', err.message);
      });
  }

  emailSignup(email: string, password: string) {
    this.afAuth.createUserWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Sucess', value);
        this.router.navigateByUrl('/profile');
      })
      .catch(error => {
        console.log('Something went wrong: ', error);
      });
  }

  async googleLogin() {
    var provider = new firebase.default.auth.GoogleAuthProvider();

    return this.oAuthLogin(provider)
      .then(value => {
        const { uid, email, displayName, photoURL, getIdToken } = value.user;
        localStorage.setItem('googleAuthUid', uid);

        this.javacodeApi.authGoogle(email, uid).subscribe(( data : { token: string } ) => {
          localStorage.setItem('apiKey', data.token);
          this.router.navigate(['/dashboard']);
        });
      })
      .catch(error => {
        console.log('Something went wrong: ', error);
      });
  }

  logout() {
    this.afAuth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }

  private oAuthLogin(provider: any) {
    return this.afAuth.signInWithPopup(provider);
  }
}
