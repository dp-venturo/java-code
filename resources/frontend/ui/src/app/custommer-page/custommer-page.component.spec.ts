import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustommerPageComponent } from './custommer-page.component';

describe('CustommerPageComponent', () => {
  let component: CustommerPageComponent;
  let fixture: ComponentFixture<CustommerPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustommerPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustommerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
