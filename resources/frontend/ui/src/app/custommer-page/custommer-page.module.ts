import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustommerPageRoutingModule } from './custommer-page-routing.module';
import { CustommerPageComponent } from './custommer-page.component';


@NgModule({
  declarations: [ CustommerPageComponent ],
  imports: [
    CommonModule,
    CustommerPageRoutingModule
  ]
})
export class CustommerPageModule { }
