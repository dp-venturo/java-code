import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustommerPageComponent } from './custommer-page.component';


const routes: Routes = [{
  path: '',
  component: CustommerPageComponent,
  data: {
    title: `Custommer Management`
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustommerPageRoutingModule { }
