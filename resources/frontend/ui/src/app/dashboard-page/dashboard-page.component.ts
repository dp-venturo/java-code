import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JavacodeService } from '../services/javacode.service';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  constructor(private route: Router, private javacodeApi: JavacodeService) { }

  ngOnInit(): void {
    // Try to get user or re-login
    this.javacodeApi.getProfile().subscribe((data: object) => {
      console.log(data);
    });
  }
}
