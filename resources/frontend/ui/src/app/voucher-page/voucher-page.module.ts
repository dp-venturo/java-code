import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VoucherPageRoutingModule } from './voucher-page-routing.module';
import { VoucherPageComponent } from './voucher-page.component';


@NgModule({
  declarations: [ VoucherPageComponent ],
  imports: [
    CommonModule,
    VoucherPageRoutingModule
  ]
})
export class VoucherPageModule { }
