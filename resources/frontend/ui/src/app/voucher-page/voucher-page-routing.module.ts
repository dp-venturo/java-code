import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VoucherPageComponent } from './voucher-page.component';


const routes: Routes = [{
  path: '',
  component: VoucherPageComponent,
  data: {
    title: `Voucher Management`
  }
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VoucherPageRoutingModule { }
