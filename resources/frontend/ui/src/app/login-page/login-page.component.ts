import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { JavacodeService } from '../services/javacode.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})


export class LoginPageComponent implements OnInit {

  constructor(public authService: AuthService, private javacodeApi: JavacodeService, private router: Router) { }

  ngOnInit(): void {
    const apiKey = localStorage.getItem('apiKey');

    if (apiKey == undefined || apiKey == "") { // User never login before
      return;
    }

    this.javacodeApi.setApiKey(apiKey);

    // Try to get user or re-login
    this.javacodeApi.getProfile().subscribe((data: object) => {
      this.router.navigate(['/dashboard']);
    });
  }

  formLogin = {
    email: "",
    password: ""
  };

  @Input() email: any;

  loginGoogle() {
    this.authService.googleLogin();
  }

  loginPassword() {
    this.javacodeApi.authPassword(this.formLogin.email, this.formLogin.password).subscribe((data: { token: string }) => {
      localStorage.setItem('apiKey', data.token);
      this.router.navigate(['/dashboard']);
    })
  }
}
