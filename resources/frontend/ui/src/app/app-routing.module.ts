import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from './default-layout/default-layout.component';
import { LoginPageComponent } from './login-page/login-page.component';


const routes: Routes = [
  {
    path: "",
    redirectTo: "/dashboard",
    pathMatch: 'full'
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./dashboard-page/dashboard-page.module').then((m) => m.DashboardPageModule)
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('./profile-page/profile-page.module').then((m) => m.ProfilePageModule)
      },
      {
        path: 'users',
        loadChildren: () =>
          import('./user-page/user-page.module').then((m) => m.UserPageModule)
      },
      {
        path: 'discount',
        loadChildren: () =>
          import('./discount-page/discount-page.module').then((m) => m.DiscountPageModule)
      },
      {
        path: 'products',
        loadChildren: () =>
          import('./product-page/product-page.module').then((m) => m.ProductPageModule)
      },
      {
        path: 'promotion',
        loadChildren: () =>
          import('./promotion-page/promotion-page.module').then((m) => m.PromotionPageModule)
      },
      {
        path: 'voucher',
        loadChildren: () =>
          import('./voucher-page/voucher-page.module').then((m) => m.VoucherPageModule)
      },
      {
        path: 'custommers',
        loadChildren: () =>
          import('./custommer-page/custommer-page.module').then((m) => m.CustommerPageModule)
      }
    ]
  },
  {
    path: "login",
    component: LoginPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
