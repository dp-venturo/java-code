<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Ui</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" href="/ui/styles.7c2f227c7d82676261e8.css"></head>
<body>
  <app-root></app-root>
<script src="/ui/runtime-es2015.06a5b1df3de361716e23.js" type="module"></script><script src="/ui/runtime-es5.06a5b1df3de361716e23.js" nomodule defer></script><script src="/ui/polyfills-es5.177e85a9724683782539.js" nomodule defer></script><script src="/ui/polyfills-es2015.f332a089ad1600448873.js" type="module"></script><script src="/ui/scripts.1e780f860bf917e0eb6e.js" defer></script><script src="/ui/main-es2015.850ddbfb89c95c1a871d.js" type="module"></script><script src="/ui/main-es5.850ddbfb89c95c1a871d.js" nomodule defer></script></body>
</html>
