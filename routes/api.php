<?php

use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// User authentication method
Route::prefix('auth')->group(function() {
    Route::post('password', [ LoginController::class, 'authPassword' ]);
    Route::post('google', [ LoginController::class, 'authGoogle' ]);
});

Route::middleware('auth:sanctum')->group(function() {
    Route::get('/profile', [ ProfileController::class, 'getProfile' ]);

    // User Management
    Route::post('user', [UserController::class, 'create']);
    Route::get('user', [UserController::class, 'all']);
    Route::get('user/{user_id}', [ UserController::class, 'show' ]);
    Route::patch('user/{user_id}', [ UserController::class, 'update' ]);
    Route::delete('user/{user_id}', [ UserController::class, 'delete' ]);
});
