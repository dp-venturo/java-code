<?php

use App\Http\Controllers\AngularController;
use App\Http\Controllers\Api\LoginController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function(Request $request){
//     return view('welcome');
// });

Route::any('/', [AngularController::class, 'index']);

Route::get("adduser", function(Request $request){
    User::create([
        'name' => "Dhuta Pratama",
        'email' => "dhutapratama@gmail.com",
        'email_verified_at' => now(),
        'password' => Hash::make("password"),
        'google_uid' => 'Eq7cpc302aVNbAh1tZNfGv0uunY2'
    ]);
});

//Route::get('login-google', [LoginController::class, 'loginGoogle']);
// Route::get('/login', [AngularController::class, 'index'])->name('login');

Route::any('/{any}', [AngularController::class, 'index'])->where('any', '^(?!api).*$');
